from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    photo = json.loads(response.content)
    try:
        return {"picture_url": photo["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather(city, state):
    params = {
        "q": city + "," + state + ",USA",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    geocode = json.loads(response.content)
    params = {
        "lat": geocode[0]["lat"],
        "lon": geocode[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    weather = json.loads(response.content)
    try:
        return {"temp": weather["main"]["temp"], "description": weather["weather"][0]["description"]}
    except:
        return {"weather": None}
